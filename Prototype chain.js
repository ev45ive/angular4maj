function Person(name){
    this.name = name
}
Person.prototype = {
	sayHello: function(){ return "I am "+this.name; }
}

function Employee(name, salary){
	Person.apply(this,arguments)
    this.salary = salary;
}

Employee.prototype = Object.create(Person.prototype)
Employee.prototype.getSalary = function(){ return this.salary }

var alice = new Employee('Alice', 4500)
var bob = new Employee('Bob', 1200)
var cat = new Employee('Cat', 0)