var path = require('path')
var ExtractText = require('extract-text-webpack-plugin')
var HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        'main':'./src/main.ts'
    },
    output:{
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
        //filename: '[name].[hash].js'
    },
    resolve:{
        extensions: ['.ts','.js']
    },
    module:{
        rules:[
            { test: /.ts$/, use:'awesome-typescript-loader' },
            { test: /.css$/, use: ExtractText.extract({
                use:'css-loader',
                fallback:'style-loader'
            })},
            { test: /.(png|jpg|svg)$/, use:{
                loader:'file-loader',
                options:{
                    name:'assets/[name].[hash].[ext]'
                }
            } },
        ]
    },
    plugins:[
        new ExtractText('styles.css'),
        new HTMLWebpackPlugin({template:'./src/index.html'})
    ],
    devtool:'sourcemap'
}