class Person{
  
  constructor(name){
    this.name = name;
  }

  sayHello(){ return this.name; }
};

class Employee extends Person{

  constructor(name,salary){
     super(name)
     this.salary = salary;
  }
  getSalary(){ return this.salary; }
}