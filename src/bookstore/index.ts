interface Book{
    id: number;
    title: string;
    author: string;
}

export default class Bookstore{

    //private books:Book[] = []

    static getGenres(){
        return []
    }

    constructor(private books:Book[] = [])
    {
        if(books && books.length){
            this.books = books;
        }
    }

    addBook(book:Book){
        this.books.push(book)
    }

    getAllBooks(){
        return this.books
    }

    findByTitle(title){
        return this.books.filter( b => b.title == title )
    }
}
