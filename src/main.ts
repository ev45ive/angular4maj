//var app = require('./app.ts')
import MyApp from './app'
import { app, secret } from './app'

import 'bootstrap/dist/css/bootstrap.css'
import './style.css'
console.log(MyApp.version)

import Bookstore from './bookstore'

Bookstore.getGenres()


var store = new Bookstore([
    {id:1, title:'Potop', author:'H.Sienkiewicz'},
    {id:2, title:'Dune', author:'F.Herbert'},
    {id:123,title:'Steve Jobs',author:'W.Isaackson'}
])

// store.addBook({id:1, title:'Potop', author:'H.Sienkiewicz'})
// store.addBook({id:2, title:'Dune', author:'F.Herbert'})
// store.addBook({id:123,title:'Steve Jobs',author:'W.Isaackson'})

//var books = store.getAllBooks()

var books = store.findByTitle('Dune')
console.log(books)

// var tquery = TitleQuery('Dune')
// var aquery = AuthorQuery('H.Sienkiewicz')
// var query = OrQuery(tquery,aquery)

// store.findByQuery( query )